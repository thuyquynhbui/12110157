﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Comment
    {
        [Key]
        public int id { get; set; }
        public int Post_id { get; set; }
        public String Body { get; set; }
    }
}