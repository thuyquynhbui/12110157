﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Tag
    {
        [Key]
        [Required(ErrorMessage = "Bắt buộc nhập")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Bắt buộc nhập")]
       // [MinLength(10, ErrorMessage = "Nhập tối thiểu 10 kí tự")]
        //[MaxLength(100, ErrorMessage = "Nhập tối đa 100 kí tự")]
            [StringLength(100,ErrorMessage="Nhập tối thiểu 10 kí tự - tối đa 100 kí tự", MinimumLength=10)]
        public String Content { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
    }
}