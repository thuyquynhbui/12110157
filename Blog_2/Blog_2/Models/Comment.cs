﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Comment
    {
        [Key]
        [Required(ErrorMessage="Bắt buộc nhập")]
        public int Id { get; set; }
        [Required(ErrorMessage="Bắt buộc nhập")]
        [StringLength(999999999, ErrorMessage = "Nhập tối thiểu 50 kí tự",MinimumLength=50)]
        public String Body { get; set; }
        [Required(ErrorMessage = "Bắt buộc nhập")]
        [DataType(DataType.Date, ErrorMessage = "Nhập ngày hợp lệ")]
        public DateTime DateCreated { get; set; }
        [Required(ErrorMessage = "Bắt buộc nhập")]
        [DataType(DataType.Date, ErrorMessage = "Nhập ngày hợp lệ")]
        public DateTime DateUpdated { get; set; }
        public int PostId { get; set; }
        public virtual Post Posts { get; set; }
        [Required(ErrorMessage="Bắt buộc nhập")]
        public String Author { get; set; }
    }
}