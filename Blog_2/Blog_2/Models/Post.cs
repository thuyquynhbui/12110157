﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Post
    {
         [Key]
         [Required(ErrorMessage = "Bắt buộc nhập")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Bắt buộc nhập")]
        [StringLength(500,ErrorMessage="Nhập ít nhất 20 kí tự - nhiều nhất 500 kí tự",MinimumLength=20)]
        public String Title { get; set; }
        [Required(ErrorMessage = "Bắt buộc nhập")]
        [StringLength(999999999,ErrorMessage="Nhập tối thiểu 50 kí tự",MinimumLength=50)]
        public String Body { get; set; }
        [Required(ErrorMessage = "Bắt buộc nhập")]
        [DataType(DataType.Date, ErrorMessage="Nhập ngày hợp lệ")]
        public DateTime DateCreated { get; set; }
        [Required(ErrorMessage = "Bắt buộc nhập")]
        [DataType(DataType.Date,ErrorMessage="Nhập ngày hợp lệ")]
        public DateTime DateUpdated { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
        [Required(ErrorMessage = "Bắt buộc nhập")]
        public int AccountId { get; set; }
        public virtual Account Accounts { get; set; }
    }
    public class BlogDataContext : DbContext
    {
        public DbSet<Post> Postss { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Tag> Tagss { get; set; }
        public DbSet<Account> Accountss { get; set; }
    }
}