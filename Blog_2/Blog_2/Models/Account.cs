﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Account
    {
        [Required(ErrorMessage = "Bắt buộc nhập")]
        [DataType(DataType.Password)]
        public String Pass { get; set; }
        [Required(ErrorMessage = "Bắt buộc nhập")]
        [EmailAddress(ErrorMessage="Nhập Email hợp lệ")]
        public String Email { get; set; }
        [Required(ErrorMessage = "Bắt buộc nhập")]
        [StringLength(100, ErrorMessage = "Nhập tối đa 100 kí tự")]
        public String FirstName { get; set; }
        [Required(ErrorMessage = "Bắt buộc nhập")]
        [StringLength(100, ErrorMessage = "Nhập tối đa 100 kí tự")]
        public String LastName { get; set; }
        [Key]
        [Required(ErrorMessage = "Bắt buộc nhập")]
        public int AcconuntId { get; set; }
        public int PostId { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
    }
}