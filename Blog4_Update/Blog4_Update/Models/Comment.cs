﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog4_Update.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        public String Body { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public int PostId { get; set; }
        public virtual Post Posts { get; set; }
        public String Author { get; set; }

    }
}