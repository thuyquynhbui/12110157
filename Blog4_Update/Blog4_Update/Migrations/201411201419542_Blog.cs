namespace Blog4_Update.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Blog : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Tag_Post", "PostId", "dbo.Posts");
            DropIndex("dbo.Tag_Post", new[] { "PostId" });
            AlterColumn("dbo.Tag_Post", "PostID", c => c.Int(nullable: false));
            DropPrimaryKey("dbo.Tag_Post", new[] { "PostId", "TagID" });
            AddPrimaryKey("dbo.Tag_Post", new[] { "PostID", "TagID" });
            AddForeignKey("dbo.Tag_Post", "PostID", "dbo.Posts", "ID", cascadeDelete: true);
            CreateIndex("dbo.Tag_Post", "PostID");
            DropColumn("dbo.Tags", "TagID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tags", "TagID", c => c.String());
            DropIndex("dbo.Tag_Post", new[] { "PostID" });
            DropForeignKey("dbo.Tag_Post", "PostID", "dbo.Posts");
            DropPrimaryKey("dbo.Tag_Post", new[] { "PostID", "TagID" });
            AddPrimaryKey("dbo.Tag_Post", new[] { "PostId", "TagID" });
            AlterColumn("dbo.Tag_Post", "PostId", c => c.Int(nullable: false));
            CreateIndex("dbo.Tag_Post", "PostId");
            AddForeignKey("dbo.Tag_Post", "PostId", "dbo.Posts", "ID", cascadeDelete: true);
        }
    }
}
