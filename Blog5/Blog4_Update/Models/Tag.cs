﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog4_Update.Models
{
    public class Tag
    {
        public int ID { get; set; }
        public string Content { get; set; }
        public virtual ICollection<Post> Posts { get; set; }

     
    }
}