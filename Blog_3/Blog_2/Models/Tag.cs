﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Tag
    {
        [Key]
        public int Id { get; set; }
        public String Content { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
    }
}