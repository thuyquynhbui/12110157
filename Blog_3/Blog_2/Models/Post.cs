﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Post
    {
         [Key]
        public int Id { get; set; }
        public String Title { get; set; }
        public String Body { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
        public int AccountId { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

    }
    public class BlogDataContext : DbContext
    {
        public DbSet<Post> Postss { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Tag> Tagss { get; set; }
    }
}