﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BaoCao.Models;

namespace BaoCao.Controllers
{
    public class InfoUserController : Controller
    {
        private HabitBdContext db = new HabitBdContext();

        //
        // GET: /InfoUser/

        public ActionResult Index()
        {
            return View(db.InfoUsers.ToList());
        }

        //
        // GET: /InfoUser/Details/5

        public ActionResult Details(string id = null)
        {
            InfoUser infouser = db.InfoUsers.Find(id);
            if (infouser == null)
            {
                return HttpNotFound();
            }
            return View(infouser);
        }

        //
        // GET: /InfoUser/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /InfoUser/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(InfoUser infouser)
        {
            if (ModelState.IsValid)
            {
                db.InfoUsers.Add(infouser);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(infouser);
        }

        //
        // GET: /InfoUser/Edit/5

        public ActionResult Edit(string id = null)
        {
            InfoUser infouser = db.InfoUsers.Find(id);
            if (infouser == null)
            {
                return HttpNotFound();
            }
            return View(infouser);
        }

        //
        // POST: /InfoUser/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(InfoUser infouser)
        {
            if (ModelState.IsValid)
            {
                db.Entry(infouser).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(infouser);
        }

        //
        // GET: /InfoUser/Delete/5

        public ActionResult Delete(string id = null)
        {
            InfoUser infouser = db.InfoUsers.Find(id);
            if (infouser == null)
            {
                return HttpNotFound();
            }
            return View(infouser);
        }

        //
        // POST: /InfoUser/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            InfoUser infouser = db.InfoUsers.Find(id);
            db.InfoUsers.Remove(infouser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}