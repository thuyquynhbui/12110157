﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BaoCao.Models
{
    public class InfoUser
    {
        [Key]
        public string UserId { get; set; }
        public virtual UserProfile UserProfiles { get; set; }
        [Display(Name="Họ tên")]
        public string FullName { get; set; }
        [Display(Name="Ngày sinh")]
        public DateTime BirthDay { get; set; }
        [Display(Name="Nghề nghiệp")]
        public string Job { get; set; }
    }
}