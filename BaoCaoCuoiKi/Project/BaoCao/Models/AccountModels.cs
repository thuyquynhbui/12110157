﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web.UI.WebControls;

namespace BaoCao.Models
{
    public class HabitBdContext : DbContext
    {
        public HabitBdContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; } 
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { set; get; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<InfoUser> InfoUsers { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>().HasMany(t => t.Tags).WithMany(p => p.Posts).
                        Map(k => k.MapLeftKey("PostID").MapRightKey("TagID").ToTable("Tag_Post"));
            modelBuilder.Entity<UserProfile>().HasMany(a => a.Events).WithMany(b => b.UserProfiles).
              Map(c => c.MapLeftKey("UserProfileUserId").MapRightKey("EventUserId").ToTable("Account_Event"));
        }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        [Display(Name = "Người đăng")]
        public string UserName { get; set; }
        [Display(Name="Email")]
        public string Email { get; set; }
        [Display(Name = "Ảnh đại diện")]
        public Image AnhDaiDien { get; set; }
        [Display(Name = "Ngày tham gia")]
        public DateTime DayCreated { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
        public virtual ICollection<Event> Events { get; set; }
    }
    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu hiện tại")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Mật khẩu ít nhất 6 kí tự", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Xác nhận mật khẩu")]
        [Compare("NewPassword", ErrorMessage = "Không khớp mật khẩu")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "Tên đăng nhập")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [Display(Name = "Ghi nhớ?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "Tên đăng nhập")]
        public string UserName { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Mật khẩu ít nhất 6 kí tự", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Xác nhận mật khẩu")]
        [Compare("Password", ErrorMessage = "Không khớp mật khẩu")]
        public string ConfirmPassword { get; set; }
        [EmailAddress(ErrorMessage="Nhập email hợp lệ")]
        
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Ảnh đại diện")]
        public Image AnhDaiDien { get; set; }
        [Display(Name = "Ngày tham gia")]
        public DateTime NgayTao { get; set; }
    }
}
