﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BaoCao.Models
{
    public class Post
    {
        [Key]
        public int ID { get; set; }
        public string Title { get; set; }
        [Display(Name = "Nội dung")]
        [Required(ErrorMessage = "Không được bỏ trống")]
        public string Body { get; set; }
        [Display(Name = "Ngày tạo")]
        public DateTime DateCreated { get; set; }
        [Display(Name = "Ngày cập nhật")]
        public DateTime DateUpdated { get; set; }
        [Display(Name = "Người đăng")]
        public int UserProfileUserId { get; set; }
        public virtual UserProfile UserProfiles { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        [Display(Name = "Tag")]
        public virtual ICollection<Tag> Tags { get; set; }
    }
}