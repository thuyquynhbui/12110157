﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BaoCao.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        [Display(Name="Bình luận")]
        public String Body { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateUpdated { get; set; }
        public string LastTime
        {
            get
            {
                if ((DateTime.Now.Day - DateUpdated.Day)>1)
                    return DateUpdated.Date.ToString() + "lúc" + DateUpdated.TimeOfDay.ToString();
                else
                    if ((DateTime.Now.Day - DateUpdated.Day)==1)
                        return "hôm qua lúc" + (DateUpdated.TimeOfDay).ToString();
                        
                    else
                        if ((DateTime.Now.Hour - DateUpdated.Hour)>0)
                            return (DateTime.Now.Second - DateUpdated.Second).ToString() + "giờ";
                        else
                            if ((DateTime.Now.Minute - DateUpdated.Minute)>0)
                                return (DateTime.Now.Minute - DateUpdated.Minute).ToString() + "phút";
                            else
                                return (DateTime.Now.Second - DateUpdated.Second).ToString() + "giây";
            }
        }
        public int PostId { get; set; }
        public virtual Post Posts { get; set; }
        [Display(Name="Tác giả")]
        public String Author { get; set; }
    }
}