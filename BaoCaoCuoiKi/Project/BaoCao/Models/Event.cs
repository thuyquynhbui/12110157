﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BaoCao.Models
{
    public class Event
    {
        public string UserId { get; set; }
        [Key]
        public string EventName { get; set; }
        public string Body { get; set; }
        public virtual ICollection<UserProfile> UserProfiles { get; set; }
    }
}